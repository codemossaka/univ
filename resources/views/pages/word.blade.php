@extends("layout.app")

@section('body')
    @isset($words)
        <div class="row">
            <div class="col-md-9">
                @include('inc.form')
            </div>
            <div class="col-md-3">
                <a class="btn btn-info" href="{{route('dangers.index')}}">Списка опасных слов</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">заметки</th>
                        <th scope="col">Процент опасности</th>
                        <th scope="col">Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($words as $word)
                        <tr>
                            <th scope="row">{{ $loop->index + 1}}</th>
                            <td>{!! $word->title !!}</td>
                            <td>{!! $word->danger !!}%</td>
                            <td>
                                <span>
                                    <form action="{{route('words.destroy',$word->id)}}" method="post">
                                        @csrf
                                        {{method_field('delete')}}
                                        <button style="cursor: pointer"><i style="color: red;" class="far fa-trash-alt"></i></button>
                                    </form>
                                </span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endisset
@endsection

@section('script')

{{--    @if(route('words.index')=='http://localhost/univ/public/words')--}}
        <script>
            $('#submit').click(function(e){
                e.preventDefault()
                var data = $('#inlineFormInputName').val()

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    method:'post',
                    url: '{{route("words.store")}}',
                    data: {data, "_token": $('#token').val()},
                    dataType: "json",
                    success: function(json) {
                        if(!json.error) location.reload(true);
                    }
                })
                location.reload(true)

            })
        </script>
    {{--@endif--}}
@endsection