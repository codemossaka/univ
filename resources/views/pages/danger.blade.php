@extends("layout.app")

@section('body')
    @isset($dangers)
        <div class="row">
            <div class="col-md-10">
                @include('inc.form', [$route])
            </div>
            <div class="col-md-2 text-center btn-group-vertical">
                <a class="btn btn-primary" href="{{route('words.index')}}">Заметки</a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-dark">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Слова</th>
                        <th scope="col">Действие</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dangers as $danger)
                        <tr>
                            <th scope="row">{{ $loop->index + 1  }}</th>
                            <td>{!! $danger->word !!}</td>
                            <td>
                                <span>
                                    <form action="{{route('dangers.destroy',$danger->id)}}" method="post">
                                        @csrf
                                        {{method_field('delete')}}
                                        <button style="cursor: pointer"><i style="color: red;" class="far fa-trash-alt"></i></button>
                                    </form>
                                </span>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    @endisset
@endsection
