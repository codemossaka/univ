<form method="post" action="{{route($route.".store")}}">
    {{csrf_field()}}
    <div class="form-row align-items-center">
        <div class="col-sm-3 my-1">
            <label class="sr-only" for="inlineFormInputName">Заметка</label>
            <input type="text" name="word" class="form-control" id="inlineFormInputName" placeholder="Добавить заметку">
        </div>
        <div class="col-auto my-1">
            <button type="submit" id="submit" class="btn btn-success">Добавить</button>
        </div>
    </div>
</form>