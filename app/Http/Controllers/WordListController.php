<?php

namespace App\Http\Controllers;
use App\WordList;
use App\Danger;
use Illuminate\Http\Request;

class WordListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $words = WordList::get();

        $route = "words";

        return view('pages.word',['words'=>$words, 'route'=>$route]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dangers = Danger::get();

        $wordToSave = [];

        if($request->ajax()){

            $data = $request->get('data');

            $array_words = explode(' ', $data);

            $length = count($array_words);

            $perc = 100/$length;

            $toSave = 0;

            foreach ($dangers as $danger){


                foreach ($array_words as $key=>$value){

                    if($danger->word === $value){
                        $array_words[$key]= "<strong>$value</strong>";

                        $toSave += $perc;

                    }

                }

            }
            $readyWords = implode(' ', $array_words);

            $words = WordList::updateOrCreate([
               'title'=>$readyWords,
               'danger'=>$toSave
            ]);

            return redirect()->route('words.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $el = WordList::find($id);
        if($el){
            $el->delete();
        }
        return redirect()->route('words.index');
    }
}
