<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Danger;
class DangerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dangers = Danger::get();

        $route = "dangers";

        return view('pages.danger',['dangers'=>$dangers, 'route'=>$route]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $danger = Danger::updateOrCreate(['word'=>$request->get('word')]);

        return redirect()->route('dangers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $el = Danger::find($id);
        if($el){
            $el->delete();
        }
        return redirect()->route('dangers.index');
    }
}
