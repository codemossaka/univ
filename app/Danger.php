<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Danger extends Model
{
    protected $table = "danger";

    protected $fillable = ['word'];

    public $timestamps = false;
}
