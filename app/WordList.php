<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WordList extends Model
{
    protected $table = "words_list";

    protected $fillable = ['title', 'danger'];

    public $timestamps = false;
}
